import 'package:dio/dio.dart';
import 'package:tokan/data/banner.dart';
import 'package:tokan/data/common/http_response_validator.dart';

abstract class IBannerDataSource {
  Future<List<BannerEntity>> getAllBanner();
}

class BannerRemoteDataSpurce
    with HttpResponseValodator
    implements IBannerDataSource {
  final Dio httpClient;

  BannerRemoteDataSpurce({required this.httpClient});
  @override
  Future<List<BannerEntity>> getAllBanner() async {
    final response = await httpClient.get('banner/slider');
    validateResponse(response);
    final List<BannerEntity> banners = [];
    (response.data as List).forEach((listBanners) {
      banners.add(BannerEntity.fromJson(listBanners));
    });
    return banners;
  }
}

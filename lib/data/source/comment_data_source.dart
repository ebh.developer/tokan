import 'package:dio/dio.dart';
import 'package:tokan/data/comment.dart';
import 'package:tokan/data/common/http_response_validator.dart';

abstract class ICommentDataSource {
  Future<List<CommentEntity>> getAllComments({required int productId});
}

class CommentRemoteDataSource
    with HttpResponseValodator
    implements ICommentDataSource {
  final Dio httpClinet;

  CommentRemoteDataSource({required this.httpClinet});
  @override
  Future<List<CommentEntity>> getAllComments({required int productId}) async {
    final response = await httpClinet.get('comment/list?product_id=$productId');
    validateResponse(response);
    final List<CommentEntity> comments = [];
    (response.data as List).forEach((comment) {
      comments.add(CommentEntity.fromJson(comment));
    });
    return comments;
  }
}

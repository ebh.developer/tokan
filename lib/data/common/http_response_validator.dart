import 'package:dio/dio.dart';
import 'package:tokan/common/exceptions.dart';

mixin HttpResponseValodator {
  validateResponse(Response response) {
    if (response.statusCode != 200) {
      throw AppException();
    }
  }
}

import 'package:tokan/common/http_client.dart';
import 'package:tokan/data/add_to_cart_response.dart';
import 'package:tokan/data/cart_response.dart';
import 'package:tokan/data/source/cart_data_source.dart';

final cartRepository =
    CartRepository(dataSource: CartRemoteDataSource(httpClient: httpClient));

abstract class ICartRepository extends ICartDataSource {}

class CartRepository implements ICartRepository {
  final ICartDataSource dataSource;

  CartRepository({required this.dataSource});
  @override
  Future<AddToCartResponse> add(int productId) => dataSource.add(productId);

  @override
  Future<AddToCartResponse> changeCount(int cartItemId) {
    // TODO: implement changeCount
    throw UnimplementedError();
  }

  @override
  Future<int> count() {
    // TODO: implement count
    throw UnimplementedError();
  }

  @override
  Future<void> delete(int cartItemId) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<CartResponse> getAll() => dataSource.getAll();
}

import 'package:tokan/common/http_client.dart';
import 'package:tokan/data/product.dart';
import 'package:tokan/data/source/product_data_source.dart';

final productRepository = ProductRepository(
    dataSource: ProductRemoteDataSource(httpClient: httpClient));

abstract class IProductRepository {
  Future<List<ProductEntity>> getAll(int sort);
  Future<List<ProductEntity>> search(String searchTerm);
}

class ProductRepository implements IProductRepository {
  final IProductDataSource dataSource;

  ProductRepository({required this.dataSource});

  @override
  Future<List<ProductEntity>> getAll(int sort) => dataSource.getAll(sort);

  @override
  Future<List<ProductEntity>> search(String searchTerm) =>
      dataSource.search(searchTerm);
}

import 'package:tokan/common/http_client.dart';
import 'package:tokan/data/comment.dart';
import 'package:tokan/data/source/comment_data_source.dart';

final commentRepository = CommentRepository(
    dataSource: CommentRemoteDataSource(httpClinet: httpClient));

abstract class ICommentRepository {
  Future<List<CommentEntity>> getAllComments({required int productId});
}

class CommentRepository implements ICommentRepository {
  final ICommentDataSource dataSource;

  CommentRepository({required this.dataSource});
  @override
  Future<List<CommentEntity>> getAllComments({required int productId}) =>
      dataSource.getAllComments(productId: productId);
}

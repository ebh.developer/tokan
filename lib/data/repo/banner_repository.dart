import 'package:tokan/common/http_client.dart';
import 'package:tokan/data/banner.dart';
import 'package:tokan/data/source/banner_data_source.dart';

final bannerRepository = BannerRepository(
    dataSource: BannerRemoteDataSpurce(httpClient: httpClient));

abstract class IBannerRepository {
  Future<List<BannerEntity>> getAllBanner();
}

class BannerRepository implements IBannerRepository {
  final IBannerDataSource dataSource;

  BannerRepository({required this.dataSource});
  @override
  Future<List<BannerEntity>> getAllBanner() => dataSource.getAllBanner();
}

import 'package:flutter/material.dart';
import 'package:tokan/common/exceptions.dart';

class AppError extends StatelessWidget {
  final AppException exception;
  final GestureTapCallback onTab;
  const AppError({
    super.key,
    required this.exception,
    required this.onTab,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(exception.message),
          ElevatedButton(onPressed: onTab, child: Text('تلاش دوباره'))
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tokan/data/comment.dart';
import 'package:tokan/data/repo/comment_repository.dart';
import 'package:tokan/theme.dart';
import 'package:tokan/ui/product/comment/bloc/commentlist_bloc.dart';
import 'package:tokan/ui/product/comment/comment.dart';
import 'package:tokan/ui/widgets/error.dart';
import 'package:tokan/ui/widgets/loading.dart';

class CommentList extends StatelessWidget {
  final int productId;
  const CommentList({super.key, required this.productId});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        CommentListBloc commentListBloc =
            CommentListBloc(commentRepository, productId);
        commentListBloc.add(CommentListStarted());
        return commentListBloc;
      },
      child: BlocBuilder<CommentListBloc, CommentListState>(
          builder: ((context, state) {
        if (state is CommentListSuccess) {
          return SliverList(
              delegate: SliverChildBuilderDelegate(
                  childCount: state.comments.length, (context, index) {
            return CommentsItem(comment: state.comments[index]);
          }));
        } else if (state is CommentListLoading) {
          return const SliverToBoxAdapter(
            child: AppLoading(),
          );
        } else if (state is CommentListError) {
          return SliverToBoxAdapter(
            child: AppError(
                exception: state.exception,
                onTab: () {
                  BlocProvider.of<CommentListBloc>(context)
                      .add(CommentListStarted());
                }),
          );
        } else {
          throw Exception('state is not supported');
        }
      })),
    );
  }
}

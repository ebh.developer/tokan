import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:tokan/common/exceptions.dart';
import 'package:tokan/data/comment.dart';
import 'package:tokan/data/repo/comment_repository.dart';

part 'commentlist_event.dart';
part 'commentlist_state.dart';

class CommentListBloc extends Bloc<CommentListEvent, CommentListState> {
  final ICommentRepository repository;
  final int productId;
  CommentListBloc(this.repository, this.productId)
      : super(CommentListLoading()) {
    on<CommentListEvent>((event, emit) async {
      if (event is CommentListStarted) {
        emit(CommentListLoading());
        try {
          final comments =
              await repository.getAllComments(productId: productId);
          emit(CommentListSuccess(comments: comments));
        } catch (e) {
          emit(CommentListError(exception: AppException()));
        }
      }
    });
  }
}

part of 'auth_bloc.dart';

sealed class AuthState extends Equatable {
  const AuthState(this.isLoginMode);
  final bool isLoginMode;

  @override
  List<Object> get props => [];
}

final class AuthInitial extends AuthState {
  const AuthInitial(super.isLoginMode);
}

class AuthError extends AuthState {
  final AppException exception;
  const AuthError(super.isLoginMode, this.exception);
}

class AuthSuccess extends AuthState {
  AuthSuccess(super.isLoginMode);
}

class AuthLoading extends AuthState {
  AuthLoading(super.isLoginMode);
}

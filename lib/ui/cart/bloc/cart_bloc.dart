import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:tokan/common/exceptions.dart';
import 'package:tokan/data/cart_response.dart';
import 'package:tokan/data/repo/cart_repository.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  final ICartRepository cartRepository;
  CartBloc(this.cartRepository) : super(CartLoading()) {
    on<CartEvent>((event, emit) async {
      if (event is CartStarted) {
        try {
          emit(CartLoading());
          final result = await cartRepository.getAll();
          emit(CartSuccess(cartResponse: result));
        } catch (e) {
          emit(CartError(exception: AppException()));
        }
      }
    });
  }
}

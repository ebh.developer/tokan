import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tokan/data/auth_info.dart';
import 'package:tokan/data/repo/auth_repository.dart';
import 'package:tokan/data/repo/cart_repository.dart';
import 'package:tokan/ui/auth/auth.dart';
import 'package:tokan/ui/cart/bloc/cart_bloc.dart';
import 'package:tokan/ui/widgets/error.dart';
import 'package:tokan/ui/widgets/image.dart';
import 'package:tokan/ui/widgets/loading.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({super.key});

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text('Cart'),
        ),
        body: BlocProvider<CartBloc>(create: (context) {
          final bloc = CartBloc(cartRepository);
          bloc.add(CartStarted());
          return bloc;
        }, child: BlocBuilder<CartBloc, CartState>(
          builder: (context, state) {
            if (state is CartLoading) {
              return const Center(child: CircularProgressIndicator());
            } else if (state is CartError) {
              return Center(child: Text(state.exception.message));
            } else if (state is CartSuccess) {
              return ListView.builder(
                itemCount: state.cartResponse.cartItems.length,
                itemBuilder: (context, index) {
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    height: 100,
                    margin: const EdgeInsets.all(4),
                    decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.surface,
                        borderRadius: BorderRadius.circular(18),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.05),
                              blurRadius: 10)
                        ]),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            // ImageLoadingService(imageUrl:  , borderRadius: borderRadius)
                          ],
                        )
                      ],
                    ),
                  );
                },
              );
            } else {
              throw Exception('current cart state is not valid');
            }
          },
        ))

        //  ValueListenableBuilder<AuthInfo?>(
        //   valueListenable: AuthRepository.authChangeNotifier,
        //   builder: (context, authState, child) {
        //     bool isAuthenticated =
        //         authState != null && authState.accessToken.isNotEmpty;
        //     return SizedBox(
        //       width: MediaQuery.of(context).size.width,
        //       child: Column(
        //         mainAxisAlignment: MainAxisAlignment.center,
        //         crossAxisAlignment: CrossAxisAlignment.center,
        //         children: [
        //           Text(isAuthenticated
        //               ? 'خوش آمدید'
        //               : 'لطفا وارد حساب کاربری خود شوید'),
        //           isAuthenticated
        //               ? ElevatedButton(
        //                   onPressed: () {
        //                     authRepository.signOut();
        //                   },
        //                   child: const Text('خروج از حساب'))
        //               : ElevatedButton(
        //                   onPressed: () {
        //                     Navigator.of(context, rootNavigator: true).push(
        //                         MaterialPageRoute(
        //                             builder: (context) => const AuthScreen()));
        //                   },
        //                   child: const Text('ورود')),
        //           ElevatedButton(
        //               onPressed: () {
        //                 authRepository.refreshToken();
        //               },
        //               child: const Text('رفرش توکن'))
        //         ],
        //       ),
        //     );
        //   },
        // ),
        );
  }
}

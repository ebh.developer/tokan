import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tokan/common/utils.dart';
import 'package:tokan/data/product.dart';
import 'package:tokan/data/repo/banner_repository.dart';
import 'package:tokan/data/repo/product_repository.dart';
import 'package:tokan/gen/assets.gen.dart';
import 'package:tokan/ui/home/bloc/home_bloc.dart';
import 'package:tokan/ui/product/product.dart';
import 'package:tokan/ui/widgets/error.dart';
import 'package:tokan/ui/widgets/loading.dart';

import 'package:tokan/ui/widgets/slider.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(create: (context) {
      final homeBloc = HomeBloc(
          bannerRepository: bannerRepository,
          productRepository: productRepository);
      homeBloc.add(HomeStarted());
      return homeBloc;
    }, child: Scaffold(
      body: SafeArea(
        child: BlocBuilder<HomeBloc, HomeState>(builder: ((context, state) {
          if (state is HomeSuccess) {
            return ListView.builder(
                physics: defaultScrollPhysics,
                itemCount: 5,
                itemBuilder: (context, index) {
                  switch (index) {
                    case 0:
                      return Container(
                        height: 56,
                        alignment: Alignment.center,
                        child: Assets.images.nikeLogo
                            .image(height: 24, fit: BoxFit.fitHeight),
                      );
                    case 2:
                      return BannerSlider(banners: state.banners);
                    case 3:
                      return _HorizontalProductList(
                        title: 'جدیدترین ها',
                        onTab: () {},
                        products: state.lastestProducts,
                      );
                    case 4:
                      return _HorizontalProductList(
                        title: 'پربازدیدترین',
                        onTab: () {},
                        products: state.lastestProducts,
                      );
                    default:
                      return Container();
                  }
                });
          } else if (state is HomeLoading) {
            return const AppLoading();
          } else if (state is HomeError) {
            return AppError(
              exception: state.exception,
              onTab: () {
                BlocProvider.of<HomeBloc>(context).add(HomeRefresh());
              },
            );
          } else {
            throw Exception('state is not a valide state');
          }
        })),
      ),
    ));
  }
}

class _HorizontalProductList extends StatelessWidget {
  final String title;
  final GestureTapCallback onTab;
  final List<ProductEntity> products;
  const _HorizontalProductList({
    super.key,
    required this.title,
    required this.onTab,
    required this.products,
  });

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding: const EdgeInsets.only(left: 12, right: 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: Theme.of(context).textTheme.titleMedium,
            ),
            TextButton(onPressed: onTab, child: const Text('مشاهده همه'))
          ],
        ),
      ),
      SizedBox(
        height: 290,
        child: ListView.builder(
            physics: defaultScrollPhysics,
            padding: const EdgeInsets.only(left: 8, right: 8),
            itemCount: products.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: ((context, index) {
              final product = products[index];
              return ProductItem(
                product: product,
                borderRadius: BorderRadius.circular(12),
              );
            })),
      )
    ]);
  }
}

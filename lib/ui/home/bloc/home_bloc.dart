import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:tokan/common/exceptions.dart';
import 'package:tokan/data/banner.dart';
import 'package:tokan/data/product.dart';
import 'package:tokan/data/repo/banner_repository.dart';
import 'package:tokan/data/repo/product_repository.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final IBannerRepository bannerRepository;
  final IProductRepository productRepository;
  HomeBloc({required this.bannerRepository, required this.productRepository})
      : super(HomeLoading()) {
    on<HomeEvent>((event, emit) async {
      if (event is HomeStarted || event is HomeRefresh) {
        emit(HomeLoading());
        try {
          final banners = await bannerRepository.getAllBanner();
          final lastestProducts =
              await productRepository.getAll(ProductSort.lastest);
          final popularProducts =
              await productRepository.getAll(ProductSort.popular);
          emit(HomeSuccess(
              banners: banners,
              lastestProducts: lastestProducts,
              popularProducts: popularProducts));
        } catch (e) {
          emit(HomeError(exception: e is AppException ? e : AppException()));
        }
      }
    });
  }
}
